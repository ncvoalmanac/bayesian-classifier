import os
from os.path import join as pj
import sys
module_path = os.path.abspath(pj('..'))
if module_path not in sys.path:
    sys.path.append(module_path)

sys.path.append(
    '/Users/marclawson/NCVO_mac/repositories/bayesian-classifier')


from naivebayesian.naivebayesian import NaiveBayesian
import configargparse
import unicodecsv as csv
import io

def main(kwargs):

    db = kwargs['db']
    column = kwargs['column']
    input_file = kwargs['input']
    output_file = kwargs['output']
    header = kwargs['header']
    result_col = kwargs['result']
    score_col = kwargs['score']
    delimiter = kwargs['delimiter']
    sqlite = kwargs['sqlite']
    mysql = kwargs['mysql']
    test = kwargs['test']
    threshold = kwargs['threshold']
    limit = kwargs['limit']

    if test:
        limit = 10

    # set up the output if none given
    if(output_file is None):
        output_file = io.StringIO()

    # stats used to track progress
    attempted_rows = 0    # the number of rows that have been attempted
    classified = 0        # the number of items we've classified
    not_classified = 0    # the number of items that haven't been classified

    print('Setting up bayesian')
    # set up the bayesian classifiers
    nb = NaiveBayesian(db)

    print('Bayesian ready...')
    # open our file and load as CSV
    with open(input_file, 'rb') as csvfile:

        if(header):
            datarows = csv.DictReader(csvfile, delimiter=delimiter)
        else:
            datarows = csv.reader(csvfile, delimiter=delimiter)
            column = int(column)
        key = 0

        nb.updateProbabilities()

        with open(output_file, 'wb') as csvoutput:
            headers = datarows.fieldnames
            headers.append(result_col)
            headers.append(score_col)
            writer  = csv.DictWriter(csvoutput, fieldnames=headers, lineterminator='\n', delimiter=delimiter)
            writer.writeheader()

            # go through each row
            for row in datarows:

                # check if we're doing a header row
                if(header==False):
                    new_row = {}
                    for k, v in enumerate(row):
                        new_row[k] = v
                    row = new_row

                # get the item we're categorising
                to_cat = None
                if(header == False or column in row):
                    to_cat = row[column]

                row_result_category = None
                row_result_score = None

                # our result
                if to_cat:
                    row_result = nb.bestMatch( to_cat, threshold )
                    if row_result:
                        classified += 1
                        row_result_category = row_result[0]
                        row_result_score = row_result[1]
                    else:
                        not_classified += 1
                    attempted_rows += 1
                    #writer.writerow( row ) # this will write out the FrID and description and an extra column called 'result' but with nothing in
                    if key % 100 == 0:
                        print('\r', key, end=',')


                row[result_col] = row_result_category
                row[score_col] = row_result_score
                writer.writerow( row ) # this row will write out the category as well as the FrID and description if 'row[score_col] = row_result_score' is commented out - they cannot be used together

                # maintain the loop
                key += 1
                if(limit > 0 and key >= limit):
                    break # if we're testing or limited then break the loop

    return classified



if __name__ == '__main__':
    main()
